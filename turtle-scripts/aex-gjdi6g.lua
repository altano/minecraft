term.clear()
print " "
print "aex - modified aEXcavate 1.0.1 (aex)" -- short for "Andre's Excavate"

-- turtlescripts.com aEXcavate for minecraft turtle bots from computer craft mod for minecraft.
-- http://pastebin.com/Kp5xGD5W
-- 
-- Written by Andre L Noel, heavily modified by Alan Norbauer
-- Creative Commons Attribution 3.0 Unported License.
-- http://creativecommons.org/licenses/by/3.0/deed.en_US
-- You may use/distribute/copy/modify/create new works with this as long as credit is given where it is due.
-- 
-- 1.0.1 Refuels w/ all provided fuel when runs out of fuel (instead of bare minimum).
-- 
-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
local tArgs = { ... }
local argOffset = 0

-- command line options flags
local enderChestFlag = 0   -- 1=enderchest is used in slot 16 to offload inventory, dedicated fuel slot 15
local leftWallFlag = 0     -- wall options are for future program development
local rightWallFlag = 0
local botFloorFlag = 2     -- 2=one wide floor, 1=full floor three wide, 0=no floor(usually not wise)
local topRoofFlag = 1      -- defaults to creating a roof
local backWallFlag = 0
local frontWallFlag = 0    -- creates a front wall with a 2 tall and 1 wide opening
local antiLiquidFlag = 1   -- creates (or not with "-water") extra anti water/lava/liquid source actions.
local useFillerBlocks = 0  -- are we using filler blocks to make walls etc

local weMoved = false -- used for detecting bedrock -- custom for aex
local failMoveMax = 20
local fuelingNow = false -- prevents recursive refueling infinite loop

 -- tracking turtles block position relative to start point
local relForward = -1 -- initial start point facing 1st block to mine
local relRight = 0
local relDeep = 0
local relFacing = 0 -- 0=forward 1=right 2=back 3=left

-- parse command line option words here
local chosenOptions = ""
local howfar = tonumber(tArgs[1 + argOffset]) 
while ( ( not ( tArgs[1 + argOffset] == nil ) ) and ( ( howfar == nil ) or ( howfar == 0 ) ) ) do -- 1st arg is not nil and not a nonzero number so its a string.
    aword = 0
    p = tArgs[1 + argOffset]

	if ( ( tArgs[1 + argOffset] == "left" ) 
		or ( tArgs[1 + argOffset] == "leftwall" ) 
		or ( tArgs[1 + argOffset] == "wallleft" ) ) then
		leftWallFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end

	if ( ( tArgs[1 + argOffset] == "right" ) 
		or ( tArgs[1 + argOffset] == "rightwall" ) 
		or ( tArgs[1 + argOffset] == "wallright" ) ) then 
		rightWallFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "floor" ) 
		or ( tArgs[1 + argOffset] == "bottom" )
		or ( tArgs[1 + argOffset] == "bot" ) ) then 
		botFloorFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "-floor" ) 
		or ( tArgs[1 + argOffset] == "-bottom" ) 
		or ( tArgs[1 + argOffset] == "-bot" ) ) then
		botFloorFlag = 0
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "top" ) 
		or ( tArgs[1 + argOffset] == "ceiling" ) 
		or ( tArgs[1 + argOffset] == "roof" ) ) then
		topRoofFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "-top" ) 
		or ( tArgs[1 + argOffset] == "-ceiling" ) 
		or ( tArgs[1 + argOffset] == "-roof" ) ) then
		topRoofFlag = 0
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "end" ) 
		or ( tArgs[1 + argOffset] == "theend" ) 
		or ( tArgs[1 + argOffset] == "back" ) 
		or ( tArgs[1 + argOffset] == "theback" )
		or ( tArgs[1 + argOffset] == "thebackend" ) 
		or ( tArgs[1 + argOffset] == "backend" )
		or ( tArgs[1 + argOffset] == "backwall" ) 
		or ( tArgs[1 + argOffset] == "thebackwall" ) 
		or ( tArgs[1 + argOffset] == "wallback" ) 
		or ( tArgs[1 + argOffset] == "walltheback" ) ) then
		backWallFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "front" ) 
		or ( tArgs[1 + argOffset] == "thefront" ) 
		or ( tArgs[1 + argOffset] == "thefrontwall" ) 
		or ( tArgs[1 + argOffset] == "frontwall" ) 
		or ( tArgs[1 + argOffset] == "face" ) ) then
		frontWallFlag = 1; useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "all" ) 
		or ( tArgs[1 + argOffset] == "allsides" ) 
		or ( tArgs[1 + argOffset] == "everyside" ) 
		or ( tArgs[1 + argOffset] == "full" ) 
		or ( tArgs[1 + argOffset] == "room" ) 
		or ( tArgs[1 + argOffset] == "every" ) ) then
		backWallFlag = 1
		frontWallFlag = 1
		leftWallFlag = 1
		rightWallFlag = 1
		botFloorFlag = 1
		topRoofFlag = 1
                useFillerBlocks = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "-all" ) 
		or ( tArgs[1 + argOffset] == "-allsides" ) 
		or ( tArgs[1 + argOffset] == "-everyside" ) 
		or ( tArgs[1 + argOffset] == "-full" ) 
		or ( tArgs[1 + argOffset] == "-room" ) 
		or ( tArgs[1 + argOffset] == "-every" ) ) then
		backWallFlag = 0
		frontWallFlag = 0
		leftWallFlag = 0
		rightWallFlag = 0
		botFloorFlag = 0
		topRoofFlag = 0
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "wall" ) -- words to ignore
		or ( tArgs[1 + argOffset] == "side" ) 
		or ( tArgs[1 + argOffset] == "walls" ) 
		or ( tArgs[1 + argOffset] == "sides" )
		or ( tArgs[1 + argOffset] == "block" )
		or ( tArgs[1 + argOffset] == "blocks" )
		or ( tArgs[1 + argOffset] == "and" )
		or ( tArgs[1 + argOffset] == "the" ) ) then
		-- ignore it
		argOffset = argOffset + 1
		aword = 1
	end
	
	if ( ( tArgs[1 + argOffset] == "enderchest" )
		or ( tArgs[1 + argOffset] == "ender" ) 
		or ( tArgs[1 + argOffset] == "chest" ) ) then
		enderChestFlag = 1
		chosenOptions = chosenOptions .. tArgs[1 + argOffset] .. " "
		argOffset = argOffset + 1
		aword = 1
	end
	
	if aword == 0 then -- not a recognized word
		print " "
		print("Error: option '" .. tArgs[1 + argOffset] .. "' is unknown.")
		print("Available options are:")
		print("  enderchest")
		print("example: > aex enderchest 25 25 -2")
		error(" ",0)
	end
	howfar = tonumber(tArgs[1 + argOffset])
end 

local howfar = tonumber(tArgs[1 + argOffset])
if howfar == nil then 
  print " "
  print "aex [<opt>] <fwd> [<right>] [<depth>]"
  print "Options: enderchest (goes in slot 16)"
  print "Place turtle facing the first block."
  print "Dig area should be forward and right."
  print "Depth should be a negative for down."
  --print "Wall options: top bottom left right back front all"
  print "Fuel goes in slot 15 then any other."
  print " "
  print "Example: > aex enderchest 24 32 -4"
  print "  digs 24 forward, 32 right, & 4 deep."
  error(" ",0)
end
if ( howfar < 1 ) then howfar = 1 end
argOffset = argOffset + 1

local howright = tonumber(tArgs[1 + argOffset])
if ( howright == nil ) then howright = howfar end -- default square
if ( howright == 0 ) then howright = 1 end
argOffset = argOffset + 1

local howdeep = tonumber(tArgs[1 + argOffset])
if ( howdeep == nil ) then howdeep=-9999 end -- default to bedrock
if ( howdeep == 0 ) then howdeep=-9999 end
howdeep = howdeep *(-1) -- impliment positive numbers for up and negative numbers for down.
argOffset = argOffset + 1

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --
function invenCheck(selection)
  local f = "stillhere" -- flag not gone to home position
  local r,d,fa,scanTo,scanFrom
  local where = ""
  -- check inventory space
  freespaces = false -- flag to see if there are any free inventory spaces
  while not freespaces do
    if enderChestFlag == 1 then scanTo = 14 else scanTo = 16 end -- if using enderchest then we dont put away our fuel in 15 or the chest in 16
    if useFillerBlocks == 1 then scanFrom = 2 else scanFrom = 1 end -- future feature
    for i=scanFrom,scanTo do
      turtle.select(i)
      if turtle.getItemCount(i) < 1 then
        freespaces = true
        break
      end
    end
    if not freespaces then
      if enderChestFlag == 1 then
        facing = relFacing -- where we are facing now
        turtle.select(16) -- can we place the chest?
        if turtle.placeUp() then where="up" else
        if turtle.placeDown() then where="down" else
        if turtle.place() then where="forward" else
        tright(); if turtle.place() then where="right" else
        tright(); if turtle.place() then where="back" else
        tright(); if turtle.place() then where="left" else
        tright() -- if nowhere then face forward again
        end end end end end end
        if not(where == "") then
          write " chest"
          for i=scanFrom,scanTo do -- put stuff in chest
            turtle.select(i)
            if      where == "up" then
              turtle.dropUp()
            else if where == "down" then
              turtle.dropDown()
            else 
              turtle.drop()
            end end
          end -- end drop loop
          turtle.select(16) -- pick up chest
          if where == "up" then
            turtle.digUp()
          else if where == "down" then
            turtle.digDown()
          else
            turtle.dig()
          end end
          faceRel(facing) -- turn back to the way we were facing
          break -- break to next while loop. retest inventory etc
        end -- end 'where' is not ""
      end -- ender chest flag is 1
      if ( where == "" ) then -- if not put in a chest already then go home
        f=relForward; r=relRight; d=relDeep; fa=relFacing
        goRel(0,0,0,0); tbw()
      end
      print " "
      print " "
      print "Turtle inventory has no free slots."
      print "Please remove items except for"
      print "any needed fuel such as coal."
      turtle.select(1)
      print "Press Enter when done."
      x = read()
    end
    if selection == nil then -- parameter allows specifying the slot selection after checking inventory
      turtle.select(1)
    else
      turtle.select(selection)
    end
  end -- while not free spaces
  if not(f=="stillhere") then tfw(); goRel(f,r,d,fa) end -- go back to work
end -- invenCheck

function fuelup()
  if fuelingNow then return end -- prevent recursive fuelup() calls
  fuelingNow = true
  local neededFuel
  local f = "stillhere" -- flag not went back to home position
  local r,d,fa,scanTo,scanFrom
  neededFuel = (4 + (math.abs(relForward) + math.abs(relRight) + math.abs(relDeep))*2) -- 4 fuel + round trip to the home position
  if enderChestFlag == 1 then scanTo = 15 else scanTo = 16 end
  if useFillerBlocks == 1 then scanFrom = 2 else scanFrom = 1 end -- future feature
  result = true
  while turtle.getFuelLevel() < neededFuel do
    result = false
    for i=scanFrom,scanTo do
      turtle.select(i)
      if turtle.refuel() then
        result = true
      end
    end -- end for slot scan loop
    if not result then
      if (f == "stillhere") and ( turtle.getFuelLevel() >= ((math.abs(relForward) + math.abs(relRight) + math.abs(relDeep))*2)+1 ) then
        f=relForward; r=relRight; d=relDeep; fa=relFacing; -- save current location and facing direction
        goRel(0,0,0,0); tbw() -- home position
      end -- set current location only once
      print " "
      print " "
      print "Please add fuel such as coal."
      print "Press Enter when done."
      x = read()
    end
  end -- while need fuel loop
  if not(f=="stillhere") then tfw(); goRel(f,r,d,fa); f="stillhere" end -- return to mining position
  fuelingNow = false
end

function digf() -- dig forward
  invenCheck()
  tried = 0
  while turtle.dig() do
    turtle.suck()
    turtle.suckUp()
    turtle.suckDown()
    if tried > 0 then
      fuelup()
      -- test if it is just liquid by moving into then out of the space once
      if turtle.forward() then
        fuelup()
        while not turtle.back() do 
          print " "
          print "Please unblock behind me so I can backup."
          print "Press Enter when done."
          x = read()
        end
        return  
      end
    end
    tried = 1
    sleep(0.3) -- necessary falling gravel time etc
  end
end

function digu() -- dig up
  invenCheck()
  tried = 0
  while turtle.digUp() do
    turtle.suckUp()
    turtle.suck()
    turtle.suckDown()
    if tried > 0 then
      fuelup()
      -- test if it is just liquid by moving into then out of the space once
      if turtle.up() then
        fuelup()
        while not turtle.down() do 
          print " "
          print "Please unblock under me so I can go down."
          print "Press Enter when done."
          x = read()
        end
        return  
      end
    end
    tried = 1
    sleep(0.3) -- necessary falling gravel time etc
  end
end

function digd() -- dig down
  invenCheck()
  tried = 0
  while turtle.digDown() do
    turtle.suckDown()
    turtle.suck()
    turtle.suckUp()
    if tried > 0 then
      fuelup()
      -- test if it is just liquid by moving into then out of the space once
      if turtle.down() then
        fuelup()
        while not turtle.up() do 
          print " "
          print "Please unblock above me so I can go up."
          print "Press Enter when done."
          x = read()
        end
        return  
      end
    end
    tried = 1
  end
end

function tleft() -- turn left and track direction facing
  turtle.suckDown()
  turtle.suck()
  turtle.suckUp()
  turtle.turnLeft()
  turtle.suck()
  relFacing = relFacing - 1
  if relFacing < 0 then relFacing = relFacing + 4 end
end

function tright() -- turn right and track direction facing
  turtle.suckDown()
  turtle.suck()
  turtle.suckUp()
  turtle.turnRight()
  turtle.suck()
  relFacing = relFacing + 1
  if relFacing > 3 then relFacing = relFacing - 4 end
end

function tab() -- turn around backwards
  turtle.suckDown()
  turtle.suck()
  turtle.suckUp()
  turtle.turnLeft()
  turtle.suck()
  turtle.turnLeft()
  turtle.suck()
  relFacing = relFacing - 2
  if relFacing < 0 then relFacing = relFacing + 4 end
end

function tfw() -- turtle move forwards
  turtle.suck()
  turtle.suckUp()
  turtle.suckDown()
  fuelup()
  failCount = 0
  while ((not turtle.forward()) and ( failCount < failMoveMax )) do -- failCount detects bedrock
    digf()
    turtle.attack()
    turtle.attack()
    weMoved = false
    failCount = failCount + 1
  end
  if ( failCount < failMoveMax ) then 
    weMoved = true
    if ( relFacing == 0 ) then -- track relative coordinates to start point
      relForward = relForward + 1
    else if ( relFacing == 2 ) then 
      relForward = relForward - 1
    else if ( relFacing == 1 ) then
      relRight = relRight + 1
    else if ( relFacing == 3 ) then
      relRight = relRight - 1
    end end end end
  end
  if not weMoved then write " Bedrock. Turtle done! Going back."; goRel(0,0,0,0); tbw(); print " Im back!"; error(" ",0); end -- CUSTOM for aex
end

function tbw() -- turtle move backwards
  turtle.suck()
  turtle.suckUp()
  turtle.suckDown()
  fuelup()
  failCount = 0
  while ((not turtle.back()) and ( failCount < failMoveMax )) do -- failCount detects bedrock
    tab()
    digf()
    turtle.attack()
    turtle.attack()
    tab()
    weMoved = false
    failCount = failCount + 1
  end
  if ( failCount < failMoveMax ) then 
    weMoved = true
    if ( relFacing == 0 ) then -- track relative coordinates to start point
      relForward = relForward - 1
    else if ( relFacing == 2 ) then 
      relForward = relForward + 1
    else if ( relFacing == 1 ) then
      relRight = relRight - 1
    else if ( relFacing == 3 ) then
      relRight = relRight + 1
    end end end end
  end
  if not weMoved then write " Bedrock. Turtle done! Going back."; goRel(0,0,0,0); tbw(); print " Im back!"; error(" ",0); end -- CUSTOM for aex
end

function tup() -- turtle move up
  turtle.suck()
  turtle.suckUp()
  turtle.suckDown()
  fuelup()
  failCount = 0
  while ((not turtle.up()) and ( failCount < failMoveMax )) do 
    digu()
    turtle.attackUp()
    turtle.attackUp()
    weMoved = false
    failCount = failCount + 1
  end
  if ( failCount < failMoveMax ) then 
    weMoved = true
    relDeep = relDeep + 1
  end
  if not weMoved then write " Bedrock. Turtle done! Going back."; goRel(0,0,0,0); tbw(); print " Im back!"; error(" ",0); end -- CUSTOM for aex
end

function tdn() -- turtle move down
  turtle.suck()
  turtle.suckUp()
  turtle.suckDown()
  fuelup()
  failCount = 0
  while ( (not turtle.down()) and ( failCount < failMoveMax )) do -- failCount detects bedrock
    digd()
    turtle.attackDown()
    turtle.attackDown()
    weMoved = false
    failCount = failCount + 1
  end
  if ( failCount < failMoveMax ) then 
    weMoved = true
    relDeep = relDeep - 1
  end
  if not weMoved then write " Bedrock. Turtle done! Going back."; goRel(0,0,0,0); tbw(); print " Im back!"; error(" ",0); end -- CUSTOM for aex
end

function faceRel(whichWay) -- turn to face the desired direction relative to start position and direction
  while not(relFacing == whichWay) do
    tleft()
  end
end

function goRel(toFw,toRight,toDeep,toFacing) -- move to position relative to start position and direction
  -- assumes not trapped under bedrock
  while ( relDeep < toDeep) do tup() end          -- up / down
  while ( relDeep > toDeep) do tdn() end
  if ( not(relRight == toRight) ) then            -- right / left
    if ( relRight < toRight ) then faceRel(1) end
    if ( relRight > toRight ) then faceRel(3) end
    while not( relRight == toRight ) do tfw() end
  end
  if ( not(relForward == toFw) ) then             -- forward / backward
    if ( relForward < toFw ) then faceRel(0) end
    if ( relForward > toFw ) then faceRel(2) end
    while not( relForward == toFw ) do tfw() end
  end
  faceRel(toFacing)                               -- facing direction  (0=fw 1=rt 2=back 3=lft)
end

function tplace(selection) -- turtle place forward
  invenCheck(selection)
  turtle.place()
end

function tplaceu(selection) -- turtle place up
  invenCheck(selection)
  turtle.placeUp()
end

function tplaced(selection) -- turtle place down
  invenCheck(selection)
  turtle.placeDown()
end

function tplacel(selection) -- turtle place left
  invenCheck(selection)
  turtle.turnLeft()
  turtle.place()
  turtle.turnRight()
end

function tplacer(selection) -- turtle place right
  invenCheck(selection)
  turtle.turnRight()
  turtle.place()
  turtle.turnLeft()
end

function tplaceb(selection) -- turtle place back
  invenCheck(selection)
  tab()
  turtle.place()
  tab()
end

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- MAIN START

print " "
write(": " .. howfar .. " forward, " .. howright .. " right, " .. math.abs(howdeep))
if ( howdeep > 0 ) then print(" going down") end
if ( howdeep < 0 ) then print(" going up") end
if not(chosenOptions == "") then print("Options: " .. chosenOptions ) end
print " "
sleep(3) -- pause to let them read before it takes off

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- MAIN LOOP

-- must use only: ( tfw tbw tup tdn; tleft tright tab ) for movement and turning (tracks relative position)

local cforeward = 0
local cright = 0
local cdeep = 0
if ( howdeep < 0 ) then updn = -1 else updn = 1 end

tfw() -- from position -1 0 0 to position 0 0 0 

write "Started"
-- depth loop begin
for cdeep = (updn),howdeep,updn do
  local foreback = true
  write(" *L" .. cdeep*updn) -- L is for layer
  -- right loop begin
  for cright = 1,howright do
    write(" R" .. cright) -- R is for row
    -- forward loop begin
    for cforward = 1,(howfar - 1) do
      write(" " .. cforward) -- block number in row
      tfw()
    end
    -- forward loop end
    write(" " .. howfar) -- block number in row (last one finished)
    if ( cright < howright ) then  -- u turn back and forth in layer
      if ( foreback == true ) then
        tright()
        tfw()
        tright()
        foreback = false
      else
        tleft()
        tfw()
        tleft()
        foreback = true
      end
    end -- u turn
  end
  -- right loop end
  actualDepth = cdeep*(-1); if updn == 1 then actualDepth = actualDepth + 1 else actualDepth = actualDepth - 1 end
  goRel(0,0,actualDepth,0) -- go to layer start position and facing direction
  write "."
  if (updn == 1) and ( cdeep < howdeep ) then tdn() else if ( cdeep > howdeep ) then tup() end end -- go into next layer
end
-- depth loop end

print " "
print " "
print "Turtle done!"
print "Going back."
goRel(0,0,0,0); tbw()
print "Im back!"
